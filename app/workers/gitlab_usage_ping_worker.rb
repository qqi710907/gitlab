# frozen_string_literal: true

class GitlabUsagePingWorker < GitlabServicePingWorker # rubocop:disable Scalability/IdempotentWorker
end
